import React, {Component} from 'react';
import './HeaderForm.css'
import Button from "../Button/Button";

class HeaderForm extends Component {


    render() {
        const {handleBtn, classHeader} = this.props
        return (
            <div className={classHeader}><span className="form-title">Do you want to delete this file?</span>
                <Button className="form-title close-btn" title="X" handleBtn={handleBtn} />
            </div>
        );
    }
}

export default HeaderForm;