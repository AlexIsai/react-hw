import React, {Component} from 'react';
import Button from "../Button/Button";
import "./Form.css"
import HeaderForm from "../HeaderForm/HeaderForm";
import FormContent from "../FormContent/FormContent";

class Form extends Component {

    render() {
        const {handleBtn, classes, classHeader, classContent, actions} = this.props
        return (
            <div className={classes}>
                <HeaderForm classHeader={classHeader} handleBtn={handleBtn}/>
                <FormContent classContent={classContent} handleBtn={handleBtn} />
                {actions.moreButton()}
            </div>
        );
    }
}

export default Form;