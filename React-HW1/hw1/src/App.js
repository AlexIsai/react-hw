
import './App.css';
import Button from "./Button/Button";
import React from 'react'

import Modal from "./Form/Form";
import "./Modal/Modal.css"
import './Button/Button.css'

class App extends React.Component {
  constructor() {
    super();

    this.state = {
      btn: {
          title: "",
          backgroundColor: ''
      },
          isOpen1: false,
          isOpen2: false

    }
  }

  showModal1 = () => {
        this.setState({isOpen1: true})
      console.log('working1', this.state.isOpen1)
    }
    showModal2 = () => {
        this.setState({isOpen2: true})
        console.log('working2', this.state.isOpen2)
    }
    closeModal =(e) => {
        if (e.target.classList.contains('modal') || e.target.tagName.toLowerCase() === "button") {
            this.setState({isOpen1: false, isOpen2: false})
            console.log(e.target)
        }
    }


  render() {
    const {isOpen1, isOpen2} = this.state

    return (
        <div className="App"  >
            {isOpen1  && <div className="modal" onClick={(e) => this.closeModal(e)}>
                <Modal
                    classes="form1"
                    classHeader="form-header1"
                    classContent="form-btn1"
                    handleBtn={(e) => this.closeModal(e)}
                    actions ={{
                        moreButton: () => (<div className="modal-button">
                            <Button className="form-btn1" title ="OK" handleBtn={(e) => this.closeModal(e)}/>
                            <Button className="form-btn1" title="CANCEL" handleBtn={(e) => this.closeModal(e)}/>
                        </div>)
                    }}
                />
            </div>}
                {isOpen2 && <div className="modal" onClick={(e) => this.closeModal(e)}>
                    <Modal
                        classes="form2"
                        classHeader="form-header2"
                        classContent="form-btn2"
                        handleBtn={() => {this.setState({isOpen2: false})}}
                        actions ={{
                            moreButton: () => (<div className="modal-button">
                                <Button className="form-btn2" title ="OK" onClick={(e) => this.closeModal(e)}/>
                                <Button className="form-btn2" title="CANCEL" onClick={(e) => this.closeModal(e)}/>
                            </div>)
                        }}
                    />
            </div>}
          <Button title="Open first modal" backgroundColor="red" handleBtn = {this.showModal1} className="base"/>
          <Button title="Open second modal" backgroundColor="blue" handleBtn = {this.showModal2} className="base"/>

        </div>

    )
  }
}

export default App;
