import React, {Component} from 'react';
import "./FormContent.css"
import Button from "../Button/Button";

class FormContent extends Component {
        state ={
            isClick: false
        }
    render() {
        const {classContent, handleBtn} = this.props
        const {isClick} = this.state
        return (
            <div className="form-content">
                <p className="text-content">Once you delete this file, it won’t be possible to undo this action.
                    Are you sure you want to delete it?</p>

                {isClick &&<div className='clicked'>
                    <h1 className="h-clickked">YOU HAVE DONE IT!!! BYE!!!</h1>
                </div>}

            </div>
        );
    }
}

export default FormContent;