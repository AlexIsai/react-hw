import React, {Component} from 'react';


class Button extends Component {

    render() {
        const {title, backgroundColor, handleBtn, className} = this.props
        return (

                <button style={{backgroundColor: backgroundColor}} onClick={handleBtn} className={className}>{title}</button>

        );
    }
}

export default Button;