import React from 'react';
import NewCard from "../../components/Card/NewCard";
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";

const Added = (props) => {
    const {items, clearAdd, isOpenModal, close, number, open} = props;


    return (
        <div className="card-container">
            {items.map(el => {
                if (el.isAdded) {
                    return <div className="cards" key={el.vendorСode}>
                        <NewCard el={el} isButton={false} addToFavorites={()=>alert("DON'T TOUCH THIS STAR")}/>
                        <Button title="X" className="added-btn" id={el.vendorСode} open={open}/>

                    </div>
                }
            })}
            <div>
                {isOpenModal && <Modal onClick={(e)=>close(e)}
                                       actions={{
                                           moreButton: () => (<div className="modal-button">
                                               <Button className="form-btn__ok" title="OK"
                                                       id={number}
                                                       open={(e) => clearAdd(e)}
                                               />
                                               <Button className="form-btn__cancel" title="CANCEL"
                                                       open={(e)=>close(e)}/>
                                           </div>)
                                       }}
                />
                }
            </div>
        </div>
    );
};

export default Added;