import React from 'react';
import NewCard from "../../components/Card/NewCard";


const Favorites = (props) => {
    const {items, addToFavorites} = props;


    return (
        <div className="card-container">
            {items.map(el => {
                if (el.isFavorite) {
                    return <div className="cards" key={el.vendorСode}>
                        <NewCard el={el} addToFavorites={addToFavorites}/>
                    </div>
                }
            })}
        </div>
    );
};

export default Favorites;