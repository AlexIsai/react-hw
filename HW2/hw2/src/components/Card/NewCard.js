import React from 'react';
import Button from "../Button/Button";
import NewStar from "../Icon/NewStar";
import "../Button/Button.scss"
import "./Card.scss"

function NewCard(props) {
    const {el, open, addToFavorites, isButton} = props
    return (
        <>

                <div className="img-container">
                    <img src={el.image} alt="cat"/>
                </div>
                <span className="cards__title">{el.name}</span>
                <NewStar color={el.isFavorite ? 'red' : 'white'} addToFavorites={addToFavorites} id={el.vendorСode}/>
                <p>Цена: {el.price}</p>
                <p>Окрас: {el.color}</p>
                {isButton && <Button title="Add to cart" className="card-btn" id={el.vendorСode} open={(e)=>open(e)}/>}

        </>
    );
}

export default NewCard;