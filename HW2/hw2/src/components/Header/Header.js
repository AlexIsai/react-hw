import React from 'react';
import {NavLink} from "react-router-dom";
import "./Header.scss"

const Header = () => {
    return (
        <div className="nav-list">
            <li><NavLink className="list-item" activeClassName="is-active" to='/cats'>Cats</NavLink></li>
            <li><NavLink className="list-item" activeClassName="is-active" to='/favorites'>Favorites</NavLink></li>
            <li><NavLink className="list-item" activeClassName="is-active" to='/added'>Added</NavLink></li>

        </div>
    );
};

export default Header;