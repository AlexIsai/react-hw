import React from 'react';
import "./ListItems.scss"
import "../Card/Card.scss"
import Button from "../Button/Button";
import NewCard from "../Card/NewCard";
import Modal from "../Modal/Modal";
import "../Card/Card.scss"

const ListItems = (props) =>  {

        const {items, open, isOpenModal, number, buy, close, addToFavorites, isButton} = props
        return (
            <>
            <div className="card-container">
                {items.map(el => {
                    return <div className="cards" key={el.vendorСode}>
                        <NewCard el={el} open={open} addToFavorites={addToFavorites} isButton={isButton}/>
                    </div>
                })}
            </div>
        <div>
                {isOpenModal && <Modal onClick={(e)=>close(e)}
                                       actions={{
                                               moreButton: () => (<div className="modal-button">
                                                       <Button className="form-btn__ok" title="OK"
                                                               id={number}
                                                               open={(e) => buy(e)}
                                                       />
                                                       <Button className="form-btn__cancel" title="CANCEL"
                                                               open={(e)=>close(e)}/>
                                               </div>)
                                       }}
                />
                }
        </div>
                    </>
        );

}



export default ListItems;