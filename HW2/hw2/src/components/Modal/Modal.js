import React from 'react';
import './Modal.scss'
import Form from "../Form/Form";
// import Button from "../Button/Button";

const Modal = (props) =>{

        const {actions, onClick} = props
        return (
            <div className="modal" onClick={(e)=>onClick(e)}>
                <Form
                    actions={actions}
                />
            </div>
        );

}

export default Modal;