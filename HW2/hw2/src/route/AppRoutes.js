import React from 'react';
import {Route, Redirect, Switch} from "react-router-dom";
import ListItems from "../components/ListItems/ListItems";
import Favorites from "../pages/Favorites/Favorites";
import Added from "../pages/Added/Added";

const AppRoutes = (props) => {
    const {items, open, isOpenModal, number, close, buy, addToCart, addToFavorites, clearAdd, isButton} = props
    return (
        <div>
            <Switch>
                <Redirect exact from="/" to="/cats" />
                <Route exect path="/cats" render={()=><ListItems
                    items={items}
                    open={open}
                    isOpenModal={isOpenModal}
                    number={number}
                    close={close}
                    buy={buy}
                    addToCart={addToCart}
                    addToFavorites={addToFavorites}
                    isButton={isButton}
                />}/>
                <Route exect path="/favorites" render={() => <Favorites
                    items={items}
                    addToFavorites={addToFavorites}

                />}/>
                <Route exect path="/added" render={() => <Added
                    items={items}
                    clearAdd={clearAdd}
                    isOpenModal={isOpenModal}
                    close={close}
                    number={number}
                    open={open}

                />}/>
            </Switch>
        </div>
    );
};

export default AppRoutes;