import './App.css';
import React, {useEffect, useState} from "react";
import axios from 'axios';
import Loading from "./components/Loading/Loading";
import Header from "./components/Header/Header";
import AppRoutes from "./route/AppRoutes";

const App = () => {
    const [items, setItems] = useState([])
    const [isLoading, setLoading] = useState(true)
    const [number, setNumber] = useState([])
    const [isOpenModal, setModal] = useState(false)
    const [isButton, setButton] = useState(true)

    const closeModal =(e) => {
        console.log(e.currentTarget.className);
        if (e.currentTarget.classList.contains('modal')) {
            setModal( false)
        }
    }

    const addedLocalStorage = (id) => {
        console.log(id);
        let arrayAdd = JSON.parse(localStorage.getItem('added')) || []
        if (!arrayAdd.includes(id)) {
            arrayAdd.push(id)
        }
        let added = JSON.stringify(arrayAdd)
        console.log(added);
        localStorage.setItem('added', added)
    }

    const addToCart = (id) => {
        console.log(id);

        const addArrAdd = items.map(el => {
            if (el.vendorСode === +id) {
                el.isAdded = true;
            }
            return el
        })
        setItems(addArrAdd)
        addedLocalStorage(id)
        setModal( false)
    }

    const removeEl = (e) => {
        let cursorTarget=e.target
        let id = +cursorTarget.id
        const addArrAdd = items.map(el => {
            if (el.vendorСode === +id) {
                el.isAdded = false;
            }
            return el
        })
        setItems(addArrAdd)
        clearAddLocalStorage(id)
        setModal( false)
    }


    const clearAddLocalStorage = (id) => {
        let arrayAdd = JSON.parse(localStorage.getItem('added')) || []
        let index = arrayAdd.indexOf(id)
        arrayAdd.splice(index, 1)
        let added = JSON.stringify(arrayAdd)
        localStorage.setItem('added', added)
    }

    const favoriteLocalStorage = (id) => {
        let arrayFav = JSON.parse(localStorage.getItem('favorites')) || []
        arrayFav = (arrayFav.includes(id) ? arrayFav.filter(el => el !== id) : arrayFav.concat(id))
        let favorites = JSON.stringify(arrayFav)
        localStorage.setItem('favorites', favorites)
    }

    const addToFavorites = (id) => {
        const addArr = items.map(el => {
            if (el.vendorСode === +id) {
                el.isFavorite = !el.isFavorite
            }
            return el
        })
        favoriteLocalStorage(id)
        setItems(addArr)
    }

    const buy = (e) => {
        let cursorTarget = e.target
        addToCart(+cursorTarget.id)
        setModal( false)
    }

    const showModal = (e) =>{
        let cursorTarget = e.target
        setNumber(cursorTarget.id)
        setModal( true)
    }

    const normData = (data) => {
        return data.map(el => {
            let favorites = JSON.parse(localStorage.getItem('favorites')) || []
            let added = JSON.parse(localStorage.getItem('added')) || []
            el.isFavorite = favorites.includes(el.vendorСode)
            el.isAdded = added.includes(el.vendorСode);
            return el
        })
    }

    useEffect(()=>{
        axios('http://localhost:3000/items.json')
            .then(res => {
                const normProp = normData(res.data)
                setItems(normProp)
                setLoading(false)
            })

    }, [])

  return (
    <div>
        {isLoading && <Loading />}

    <div className="App">
        <Header />
        <AppRoutes
            items={items}
            open={showModal}
            isOpenModal={isOpenModal}
            number={number}
            close={closeModal}
            buy={buy}
            addToCart={addToCart}
            addToFavorites={addToFavorites}
            clearAdd={removeEl}
            isButton={isButton}
        />
    </div>
    </div>
  );
}

export default App;


