import {
    CHANGE_ADD_STATUS,
    CHANGE_FAV_STATUS, CHECKOUT,
    CLOSE_MODAL,
    ITEMS_LOADING,
    LOAD_ITEMS,
    OPEN_MODAL,
    SET_NUMBER,
    SUBMIT_FORM,
} from "./types";


export const setItems = (data) => {
    return {type: LOAD_ITEMS, payload: data}
}

export const setLoading = (data) => {
    return {type: ITEMS_LOADING, payload: data}
}

export const openModal = (data) => {
    return {type: OPEN_MODAL, payload: data}
}

export const closeModal = (data) => {
    return {type: CLOSE_MODAL, payload: data}
}

export const addFavor = (data) => {
    return {type: CHANGE_FAV_STATUS, payload: data}
}

export const setNumber = (data) => {
    return {type: SET_NUMBER, payload: data}
}

export const addCart = (data) => {
    return {type: CHANGE_ADD_STATUS, payload: data}
}

export const deleteCartElement = (data) => {
    return {type: CHANGE_ADD_STATUS, payload: data}
}

export const fillData = (data) => {
    return {type: SUBMIT_FORM, payload: data}
}

export const checkout = (data) => {
    return {type: CHECKOUT, payload: data}
}