import axios from "axios";
import {
    addCart,
    addFavor, checkout,
    closeModal,
    deleteCartElement,
    fillData,
    openModal,
    setItems,
    setLoading,
    setNumber,
} from "./actions";




export const getItems = () => (dispatch) => {
    axios('http://localhost:3000/items.json')
        .then(res => {
            const normProp = normData(res.data)
            dispatch(setItems(normProp))
            dispatch(setLoading(false))
        })
}

export const showModal = (e) =>(dispatch) => {
    let cursorTarget = e.target
    dispatch(setNumber(cursorTarget.id))
    dispatch(openModal( true))
}



export const hideModal = (e) => (dispatch) =>  {
    if (e.currentTarget.classList.contains('modal')) {
        dispatch(closeModal( false))
    }
}



const normData = (data) => {
    return data.map(el => {
        let favorites = JSON.parse(localStorage.getItem('favorites')) || []
        let added = JSON.parse(localStorage.getItem('added')) || []
        el.isFavorite = favorites.includes(el.vendorСode)
        el.isAdded = added.includes(el.vendorСode);
        return el
    })
}

const favoriteLocalStorage = (id) => {
    let arrayFav = JSON.parse(localStorage.getItem('favorites')) || []
    arrayFav = (arrayFav.includes(id) ? arrayFav.filter(el => el !== id) : arrayFav.concat(id))
    let favorites = JSON.stringify(arrayFav)
    localStorage.setItem('favorites', favorites)
}

export const addToFavorites = (id, items) => (dispatch) => {
    const addArr = items.map(el => {
        if (el.vendorСode === +id) {
            el.isFavorite = !el.isFavorite
        }
        return el
    })
    favoriteLocalStorage(id)
    dispatch(addFavor(addArr))
}

export const buy = (e, items) => (dispatch) => {

    let cursorTarget = e.target
    dispatch(addToCart(+cursorTarget.id, items))
}

const addedLocalStorage = (id) => {
    let arrayAdd = JSON.parse(localStorage.getItem('added')) || []
    if (!arrayAdd.includes(id)) {
        arrayAdd.push(id)
    }
    let added = JSON.stringify(arrayAdd)
    localStorage.setItem('added', added)
}

const addToCart = (id, items) => (dispatch) => {
    const addArrAdd = items.map(el => {
        if (el.vendorСode === +id) {
            el.isAdded = true;
        }
        return el
    })
    addedLocalStorage(id)
    closeModal( false)
    dispatch(addCart(addArrAdd))
}

export const removeEl = (e, items) => (dispatch) => {
    let cursorTarget=e.target
    let id = +cursorTarget.id
    const addArrAdd = items.map(el => {
        if (el.vendorСode === +id) {
            el.isAdded = false;
        }
        return el
    })
    dispatch(deleteCartElement(addArrAdd))
    clearAddLocalStorage(id)
}


const clearAddLocalStorage = (id) => {
    let arrayAdd = JSON.parse(localStorage.getItem('added')) || []
    let index = arrayAdd.indexOf(id)
    arrayAdd.splice(index, 1)
    let added = JSON.stringify(arrayAdd)
    localStorage.setItem('added', added)
}

export const clearLocalStorageCart = (items) => (dispatch) => {
    items.map(elem => {
           elem.isAdded = false;
       return elem
    })
    localStorage.removeItem('added')
}

export const getOrder = (items) => (dispatch) => {
    let arrOrder = items.filter(e=>e.isAdded)
    console.log(arrOrder);
    return arrOrder
}

export const userData = (values) => (dispatch) => {
    dispatch(fillData(values))
};

export const checkoutCart = (data) => (dispatch) => {
    dispatch(checkout(data))
};