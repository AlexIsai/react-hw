import {
    CHANGE_ADD_STATUS,
    CHANGE_FAV_STATUS, CHECKOUT,
    CLOSE_MODAL,
    ITEMS_LOADING,
    LOAD_ITEMS,
    OPEN_MODAL,
    SET_NUMBER, SUBMIT_FORM,
} from "./types";


const initialState = {
    items: {
        data: [],
        isLoading: true
    },
    isOpenModal: false,
    number: [],
    isFavorite: false,
    userInform: {
        data: {},
        isOpenForm:false
    },
    isButtonAdd: true,
    isButtonCheckout: true
}


const reducer = (state = initialState, action) => {
    switch (action.type){
        case LOAD_ITEMS:
            return {...state, items: {...state.items, data: action.payload}}
        case ITEMS_LOADING:
            return {...state, items: {...state.items, isLoading: action.payload}}
        case OPEN_MODAL:
            return {...state, isOpenModal: action.payload}
        case CLOSE_MODAL:
            return {...state, isOpenModal: action.payload}
        case SET_NUMBER:
            return {...state, number: action.payload}
        case CHANGE_FAV_STATUS:
            return {...state, items: {...state.items, data: action.payload}}
        case CHANGE_ADD_STATUS:
            return {...state, items: {...state.items, data: action.payload}}
        case CHECKOUT:
            return {...state, userInform: {...state.userInform, isOpenForm: true}, isButtonCheckout: false }
        case SUBMIT_FORM:
            return {...state,
                userInform: {...state.userInform, data: {...action.payload}}}

        default:
            return state
    }

}


export default reducer