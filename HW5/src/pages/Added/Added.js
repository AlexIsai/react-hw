import React from 'react';
import NewCard from "../../components/Card/NewCard";
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import {checkoutCart, clearLocalStorageCart, hideModal, removeEl, showModal} from "../../store/operations";
import {connect} from "react-redux";
import FormikCart from "../../components/FormikCart/FormikCart";

const Added = (props) => {
    let arrCart = props.items.data.filter(e => e.isAdded)
    return (
        <div>
            {(arrCart.length === 0) &&
            <h2 className="error">NOTHING HAVE BEEN ADDED!</h2>
            }
        <div className="card-container">

            {arrCart.map(el => {
                    return <div className="cards" key={el.vendorСode}>
                        <NewCard el={el} isButtonAdd={false}/>
                        <Button title="X" className="added-btn" id={el.vendorСode} open={props.viewModal}/>
                    </div>
            })}
        </div>
            <div>
                {props.isOpenModal && <Modal onClick={(e)=>props.closeModal(e)}
                                       actions={{
                                           moreButton: () => (<div className="modal-button">
                                               <Button className="form-btn__ok" title="OK"
                                                       id={props.number}
                                                       open={(e) => props.deleteEl(e, props.items)}
                                               />
                                               <Button className="form-btn__cancel" title="CANCEL"
                                                       open={(e)=>props.viewModal(e)}/>
                                           </div>)
                                       }}
                />
                }
            </div>

            {(arrCart.length !== 0) && (props.isButtonCheckout) &&
            <div className="div-state">
                <Button title="CHECKOUT" open={()=>props.checkout(true)}/>
            </div>}
            {props.userInform.isOpenForm && arrCart.length !== 0 &&
            <div className="formik">
            <FormikCart />
        </div>
            }
            {/*{!props.userInform.isOpenForm &&*/}
            {/*<h2 className="error">NOTHING HAVE BEEN ADDED!</h2>*/}
            {/*}*/}
        </div>
    );
};



const mapStateToProps =(state) => {
    return {
        items: state.items,
        isOpenModal: state.isOpenModal,
        number: state.number,
        userInform: state.userInform,
        isButtonCheckout: state.isButtonCheckout
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        viewModal: (e) => dispatch(showModal(e)),
        closeModal: (e) => dispatch(hideModal(e)),
        deleteEl: (e, items) => dispatch(removeEl(e, items.data)),
        clearLocalStorageAdd: () => dispatch(clearLocalStorageCart()),
        checkout: (data) => dispatch(checkoutCart(data))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Added);
