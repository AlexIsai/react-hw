import './App.css';
import React, {useEffect} from "react";
import Loading from "./components/Loading/Loading";
import Header from "./components/Header/Header";
import AppRoutes from "./route/AppRoutes";
import {getItems} from "./store/operations";
import {connect} from "react-redux";

const App = (props) => {
    useEffect(() => {
        props.getItems()
    }, [props])
    return (
        <div>
            {props.items.isLoading && <Loading/>}
            <div className="App">
                <Header/>
                <AppRoutes/>
            </div>
        </div>
    );
}
const mapStateToProps = (state) => {
    return {
        items: state.items,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        getItems: () => dispatch(getItems()),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);






