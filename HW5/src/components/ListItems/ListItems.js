import React from 'react';
import "./ListItems.scss"
import "../Card/Card.scss"
import Button from "../Button/Button";
import NewCard from "../Card/NewCard";
import Modal from "../Modal/Modal";
import "../Card/Card.scss"
import {connect} from "react-redux";
import {buy, hideModal} from "../../store/operations";

const ListItems = (props) => {
    return (
        <>
            <div className="card-container">
                {props.items.data.map(el => {
                    return <div className="cards" key={el.vendorСode}>
                        <NewCard
                            el={el}
                        />
                    </div>
                })}
            </div>
            <div>
                {props.isOpenModal && <Modal
                    actions={{
                        moreButton: () => (<div className="modal-button">
                            <Button className="form-btn__ok" title="OK"
                                    id={props.number}
                                    open={(e) => props.buyCat(e, props.items)}
                            />
                            <Button className="form-btn__cancel" title="CANCEL"
                                    open={(e) => props.closeModal(e)}/>
                        </div>)
                    }}
                />
                }
            </div>
        </>
    );
}
const mapStateToProps = (state) => {
    return {
        items: state.items,
        isOpenModal: state.isOpenModal,
        number: state.number,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        closeModal: (e) => dispatch(hideModal(e)),
        buyCat: (id, items) => dispatch(buy(id, items.data)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListItems);




