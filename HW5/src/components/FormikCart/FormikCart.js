import React from 'react';
import * as Yup from 'yup';
import {Formik, Form} from "formik";
import MyInput from "./MyInput";
import {clearLocalStorageCart, getOrder, userData} from "../../store/operations";
import {connect} from "react-redux";

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/


const validationFormSchema = Yup.object().shape({
    name: Yup.string()
        .required('Required')
        .max(30, 'Too long to ba a name')
        .min(3, 'Too short to be a name'),
    secondName: Yup.string()
        .required('Required')
        .max(30, 'Too long to ba a surname')
        .min(2, 'Too short to be a surname'),
    age: Yup.string()
        .required('Required')
        .matches(/^[0-9]*$/)
        .min(1, 'Too young')
        .max(2, 'Too old'),
    address: Yup.string()
        .required('Required')
        .min(10, 'Too Short to be an address'),
    phone: Yup.string()
        .required('Required')
        .matches(phoneRegExp, 'Phone number is not valid')

})


const FormikCart = (props) => {
    const submitForm = (values) => {

        const {name, secondName, age, address, phone} = values
        console.log('SUBMIT FORM:', name, secondName, age, address, phone)
        props.getOrder(props.items)
        props.clearLocalStorageAdd(props.items)
        props.getUserData(values)
    };

    return (
        <div className="forma">
            <h3>Input your contacts:</h3>
                <Formik
                    initialValues={{
                    name: "",
                    secondName: "",
                    age: "",
                    address: "",
                    phone: "",
                }}
                    onSubmit={submitForm}
                    validationSchema={validationFormSchema}
                >
                    {(formikProps) => {
                        return (
                            <Form>
                                <MyInput name="name" type="text" label="Name" />
                                <MyInput name="secondName" type="text" label="Second name" />
                                <MyInput name="age" type="number" label="Age" />
                                <MyInput name="address" type="text" label="Address" />
                                <MyInput name="phone" type="number" label="Phone" />

                                <div>
                                    <button type="submit">SUBMIT</button>
                                </div>
                            </Form>
                        );
                    }}
                </Formik>
        </div>
    );
}



const mapStateToProps =(state) => {
    return {
        items: state.items,
        user: state.user
        // isOpenModal: state.isOpenModal,
        // number: state.number,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        // viewModal: (e) => dispatch(showModal(e)),
        // closeModal: (e) => dispatch(hideModal(e)),
        // deleteEl: (e, items) => dispatch(removeEl(e, items.data)),
        clearLocalStorageAdd: (items) => dispatch(clearLocalStorageCart(items.data)),
        getOrder: (items) => dispatch(getOrder(items.data)),
        getUserData: (values) => dispatch(userData(values))
        // clearField: (values) => dispatch(clearForm(values))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(FormikCart);

// export default FormikCart;