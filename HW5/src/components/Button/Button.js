import React from 'react';
import "./Button.scss"

const Button = (props) => {
        const {title, open, id, className} = props
        return (
            <button id={id} className={className} onClick={open}>{title}</button>
        );

}

export default Button;