import React from 'react';
import "./Form.scss"

const Form = (props) => {

        const {actions} = props
        return (
            <div className="form">
                <div className="form-title"> ARE YOU SURE?</div>
                {actions.moreButton()}
            </div>
        );

}

export default Form;