import React from 'react';
import Button from "../Button/Button";
import NewStar from "../Icon/NewStar";
import "../Button/Button.scss"
import "./Card.scss"
import {connect} from "react-redux";
import {showModal} from "../../store/operations";

function NewCard(props) {
    let {el} = props
    return (
        <>
            <div className="img-container">
                <img src={el.image} alt="cat"/>
            </div>
            <span className="cards__title">{el.name}</span>
            <NewStar color={el.isFavorite ? 'red' : 'white'} id={el.vendorСode}/>
            <p>Цена: {el.price}</p>
            <p>Окрас: {el.color}</p>
            {props.isButtonAdd &&
            <Button title="Add to cart" className="card-btn" id={el.vendorСode} open={(e) => props.viewModal(e)}/>}
        </>
    );
}
const mapStateToProps = (state) => {
    return {
        items: state.items,
        isOpenModal: state.isOpenModal,
        number: state.number,
        isButtonAdd: state.isButtonAdd
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        viewModal: (e) => dispatch(showModal(e)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(NewCard);







