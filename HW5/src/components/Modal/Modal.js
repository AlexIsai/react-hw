import React from 'react';
import './Modal.scss'
import Form from "../Form/Form";
import {connect} from "react-redux";
import {hideModal} from "../../store/operations";

const Modal = (props) =>{

        const {actions} = props

        return (
            <div className="modal" onClick={(e)=>props.closeModal(e)}>
                <Form
                    actions={actions}
                />
            </div>
        );

}

const mapStateToProps =(state) => {
        return {
        }
}

const mapDispatchToProps = (dispatch) => {
        return {
                closeModal: (e) => dispatch(hideModal(e))
        }
}


export default connect(mapStateToProps, mapDispatchToProps)(Modal);


