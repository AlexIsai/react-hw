import React from 'react';
import {Route, Redirect, Switch} from "react-router-dom";
import ListItems from "../components/ListItems/ListItems";
import Favorites from "../pages/Favorites/Favorites";
import Added from "../pages/Added/Added";

const AppRoutes = () => {
    return (
        <div>
            <Switch>
                <Redirect exact from="/" to="/cats"/>
                <Route exect path="/cats" render={() => <ListItems/>}/>
                <Route exect path="/favorites" render={() => <Favorites/>}/>
                <Route exect path="/added" render={() => <Added/>}/>
            </Switch>
        </div>
    );
};

export default AppRoutes;